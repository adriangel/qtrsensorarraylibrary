/*
    QTRSensorsCustomLib.h - Library to use the QTR Sensor Array
    Created by Adrian Villarroel, August 27, 2018
    Released into the public domain
*/
#ifndef QTRSensorArray_h
#define QTRSensorArray_h

#include <Arduino.h>
/** Contains the methods to calibrate and read the QTR sensor Array
  * 
  * 
  * 
  */
class QTRSensorArray{
    private:
        unsigned int* pins_array;
        unsigned int emitter_pin;               
        unsigned int size;  
        unsigned int mid_sensor;  
    public:
        int* weights_array;
        int* max_bound_reads;
        int* min_bound_reads;
        unsigned int* reads_array;
        unsigned int* mapped_reads_array;
        int position; 
        float tolerance_fraction;
        QTRSensorArray();
        QTRSensorArray(unsigned int* pins_arr,unsigned int sz,float tolerance_fraction,unsigned int emitter_pin);        
        void QTRSensorArrayInit(unsigned int* pins_arr,unsigned int sz,float tolerance_fraction,unsigned int emitter_pin);
        void QTRSensorCalibrate();
        int* mapReads();
        int calculatePosition();
};
#endif