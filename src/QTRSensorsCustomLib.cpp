#include "Arduino.h"
#include "QTRSensorsCustomLib.h"


QTRSensorArray::QTRSensorArray(){
    
}


QTRSensorArray::QTRSensorArray(unsigned int* pins_arr,unsigned int sz,float tol_fract,unsigned int em_pin):pins_array(pins_arr),size(sz),tolerance_fraction(tol_fract),emitter_pin(em_pin){
    QTRSensorArrayInit(pins_arr,sz,tol_fract,em_pin);     
}



void QTRSensorArray::QTRSensorArrayInit(unsigned int* pins_arr,unsigned int sz,float tol_fract,unsigned int em_pin){
    pins_array=pins_arr;
    size=sz;
    pinMode(emitter_pin,OUTPUT);
    digitalWrite(emitter_pin,HIGH);
    weights_array=(int*)calloc(size,sizeof(int));
    reads_array=(unsigned int *)calloc(size,sizeof(unsigned int));
    mapped_reads_array=(unsigned int*)calloc(size,sizeof(unsigned int));
    tolerance_fraction=tol_fract;
    if (size % 2 ==0){
        unsigned int start_end=size/2;
        for(int i=0;i<size;i++){
            if(i<=start_end-1)
                weights_array[i]=-1*(start_end-i);
            else
                weights_array[i]=(i-start_end)+1;
        }
    } else {
        int start_end=(size-1)/2;
        for(int i=-start_end;i<=start_end;i++){
                weights_array[i+start_end]=i;   
        }
    }

    if(size % 2==0)
        mid_sensor=size/2-1;
    else
        mid_sensor=(size+1)/2;    
}

/** 
 * This algorithm is used to calibrate the minimum and maximum ADC values 
 * for each sensor. It assumes that if the sensor is out of the line, the value 
 * read by the ADC will be less than noise_band. On the other hand, if the read
 * value is more than 1023-on_line_band, the sensor is on the line.
 * 
 */ 

void  QTRSensorArray::QTRSensorCalibrate(){    

    int delay_millis=100;
    int repetitions=100;
    int pre_mins[size]={};
    int pre_maxs[size]={};
    int* mins=(int*)calloc(size,sizeof(int));
    int* maxs=(int*)calloc(size,sizeof(int));
    int read_value;
   
    for(int i=0; i<repetitions;i++){
        for(int j=0;j<size;j++){
            read_value=analogRead(pins_array[j]);           
            if(i==0){
                pre_mins[j]=read_value;                
            } else {
                if(read_value<pre_mins[j])
                    pre_mins[j]=read_value;
                else if(read_value>pre_mins[j] && read_value>pre_maxs[j]){
                    pre_maxs[j]=read_value;                    
                }
            }
        }
        delay(delay_millis);          
    }
   
    for(unsigned int i=0;i<size;i++){
        float fraction=0.2;
        int tolerance=(int) round((float)abs(pre_maxs[i]-pre_mins[i])*fraction);       
        mins[i]=pre_mins[i]+tolerance;      
        maxs[i]=pre_maxs[i]-tolerance;        
    }
    max_bound_reads=maxs;
    min_bound_reads=mins;
}

int* QTRSensorArray::mapReads(){   
    for(unsigned int i=0;i<size;i++){
        reads_array[i]=analogRead(pins_array[i]);
        int mapped_val=(int) map((long)reads_array[i],(long)min_bound_reads[i],(long)max_bound_reads[i],0,1000);
        mapped_reads_array[i]=constrain(mapped_val,0,1000);
    }
    return mapped_reads_array;
}

int QTRSensorArray::calculatePosition(){
    int position=0;
    int extreme_position=7000;
    float percentage_by_left_edge_sensor;
    float percentage_by_right_edge_sensor;
    float percentage_threshold_by_edge_sensor=0.7;
    mapped_reads_array=mapReads();
   
   
    for(unsigned int i=0;i<size;i++){           
        int position_term=weights_array[i]*mapped_reads_array[i];
        position=position+position_term;
    }

    if(mapped_reads_array[size-1]>mapped_reads_array[size-2] && mapped_reads_array[size-1]>mapped_reads_array[mid_sensor]){
        percentage_by_left_edge_sensor=(float) mapped_reads_array[size-1]*weights_array[size-1]/position;
        if(percentage_by_left_edge_sensor>percentage_threshold_by_edge_sensor)
            position=extreme_position;
    } 
    
    if(mapped_reads_array[0]>mapped_reads_array[1] && mapped_reads_array[0]>mapped_reads_array[mid_sensor] ){
        percentage_by_right_edge_sensor= abs((float)mapped_reads_array[0]*weights_array[0]/position);
        if(percentage_by_right_edge_sensor>percentage_threshold_by_edge_sensor)
            position=-extreme_position;
    }       
    return position;
}