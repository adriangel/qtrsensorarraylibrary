/*
 * Test the QTRSensorsCustomLib *
 */ 


#include <Arduino.h>
#include "QTRSensorsCustomLib.h"

QTRSensorArray sens_arr;
unsigned int SENSOR_PINS[]={0,1,2,3,4,5,6,7};
unsigned int size;
unsigned int emitter_pin=2;
float tolerance_fraction=0.2;
int position;

void setup() {    
    // put your setup code here, to run once:    
    Serial.begin(9600);    
    size=sizeof(SENSOR_PINS)/sizeof(*SENSOR_PINS);
    sens_arr=QTRSensorArray(SENSOR_PINS,size,tolerance_fraction,emitter_pin);  
    sens_arr.QTRSensorCalibrate();  
    
}

void loop() { 
    position=sens_arr.calculatePosition();
    Serial.print("Max bounds:   "); 
    for(unsigned int i=0;i<size;i++){
        Serial.print(sens_arr.max_bound_reads[i]);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("Raw reads:    ");   
    for(unsigned int i=0;i<size;i++){          
        Serial.print(sens_arr.reads_array[i]);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("Min bounds:   ");    
    for(unsigned int i=0;i<size;i++){         
        Serial.print(sens_arr.min_bound_reads[i]);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("Mapped reads: ");    
    for(unsigned int i=0;i<size;i++){         
        Serial.print(sens_arr.mapped_reads_array[i]);
        Serial.print(" ");
    }
    Serial.print("Position: ");
    Serial.println(position);    
    delay(500);
}